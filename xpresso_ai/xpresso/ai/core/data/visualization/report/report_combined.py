import os
import shutil
import uuid
from xpresso.ai.core.data.visualization import utils
from xpresso.ai.core.data.automl import utility
from xpresso.ai.core.commons.utils.constants import REPORT_OUTPUT_PATH, \
    EMPTY_STRING
from xpresso.ai.core.data.visualization.report import UnivariateReport, \
    MultivariateReport, ReportParam


class CombinedReport(UnivariateReport, MultivariateReport):
    def __init__(self, dataset):
        super().__init__(dataset)

    def create_report(self, input_path=utils.DEFAULT_IMAGE_PATH,
                      output_path=REPORT_OUTPUT_PATH, file_name=None,
                      report_format=ReportParam.SINGLEPAGE.value):
        """
        Function to create the report for the metrics of all the attributes
        Args:
            input_path (str): path to the plots to be fetched into the report
            output_path (str): path to the folder the report to be saved
            report_format (:obj: `ReportParam`): whether the attribute
            distribution should be on same page or continued on different pages
            file_name(str): file name of report file
        """
        if file_name is None or file_name == EMPTY_STRING:
            file_name = "report_combined_{}.pdf".format(
                str(uuid.uuid4()))
        output_path, output_file_name = utility.get_file_from_path_string(
            output_path,
            file_name)
        if output_path is None:
            output_path = REPORT_OUTPUT_PATH
        if not os.path.exists(output_path):
            os.mkdir(output_path)

        output_file_name = utility.set_extension(output_file_name, ".pdf")
        self.title_page()
        self.overview_page()
        self.add_univariate_plots(input_path=utils.DEFAULT_IMAGE_PATH,
                                  report_format=report_format,
                                  plot_scatter=True)
        self.add_multivariate_plots(input_path=utils.DEFAULT_IMAGE_PATH)
        self.output(os.path.join(output_path, output_file_name), 'F')

        if os.path.exists(input_path):
            shutil.rmtree(input_path)
