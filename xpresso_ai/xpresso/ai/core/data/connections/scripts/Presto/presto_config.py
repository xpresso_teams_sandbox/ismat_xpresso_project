"""Module to specify configurations for setting up Presto locally"""

# PRESTO CONFIG
# To know more about how to write catalogs to connect to various databases visit,
# http://prestodb.github.io/docs/current/connector.html

presto_catalogs = {
  "sqlserver.properties": "connector.name=sqlserver\nconnection-url=jdbc:sqlserver://223.31.198.244:1433;"
                          "databaseName=annotationsdb\nconnection-user=sa\nconnection-password=dbsql",
  "mongodb.properties": "connector.name=mongodb\nmongodb.seeds=172.16.6.1,host:27017",
  "mysql.properties": "connector.name=mysql\nconnection-url=jdbc:mysql://example.net:3306\nconnection-user=root"
                      "\nconnection-password=secret",
  "cassandra.properties": "connector.name=cassandra\ncassandra.contact-points=172.16.2.5\ncassandra.username"
                          "=cassandra\ncassandra.password=cassandra "
}